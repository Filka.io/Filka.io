import pandas as pd
import json

f = open("testJSON/dataJSON.json")

dataJSON = json.load(f)
"""
Function for reaching stats responsible for hours eg. last 6hrs, last 12hrs, last 24hrs etc.
"""


def getHourStats(json, hours):

    df = pd.DataFrame.from_records(dataJSON)
    df["time"] = pd.to_datetime(df["date_timestamp"], unit="s")
    df["date_timestamp"] = pd.to_numeric(df["date_timestamp"])
    df["temperature"] = pd.to_numeric(df["temperature"])
    df["humidity"] = pd.to_numeric(df["humidity"])
    df.set_index("date_timestamp")
    time = hours * 60 * 60
    stats = df.loc[df.date_timestamp > df.iloc[0]["date_timestamp"] - time, :]
    # TODO: Finish combining the function


df = pd.DataFrame.from_records(dataJSON)
df["time"] = pd.to_datetime(df["date_timestamp"], unit="s")
df["date_timestamp"] = pd.to_numeric(df["date_timestamp"])
df["temperature"] = pd.to_numeric(df["temperature"])
df["humidity"] = pd.to_numeric(df["humidity"])
df.set_index("date_timestamp")

time = 24 * 60 * 60


stats = df.loc[
    df.date_timestamp > df.iloc[0]["date_timestamp"] - time, :
]  # reaching stats from desire time period

dataVals = [
    "min",
    "max",
    "mean",
    "std",
    "median",
]  # desired values from ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max']

agregatedStats = stats.agg({"temperature": dataVals, "humidity": dataVals})
foo = df.loc[df["temperature"] == agregatedStats["temperature"]["max"]]


print(agregatedStats.to_json())
data = {}
params = {}
dirData = []
for label in range(len(agregatedStats.columns)):
    # print(list(agregatedStats.columns))
    for x in range(len(agregatedStats.index)):
        column = agregatedStats.columns[label]
        print(f"column={column}")
        row = agregatedStats.index[x]
        foo = df.loc[
            df[column] == agregatedStats[column][row]
        ]  # localize time for desired valuexs
        if agregatedStats.index[x] in ["max", "min"]:
            params[agregatedStats.index[x]] = {
                "value": int(agregatedStats[column][row]),
                "time": int(foo["date_timestamp"].values[0]),
            }
        else:
            params[agregatedStats.index[x]] = int(
                agregatedStats[agregatedStats.columns[label]][row]
            )

    data[agregatedStats.columns[label]] = params

    dirData.append(str(data))

data = {}
for label in list(agregatedStats.columns):
    params = {}
    for row in agregatedStats.index:
        if row in ["max", "min"]:
            time = df.loc[
                df[column] == agregatedStats[column][row]
            ]  # localize time for desired valuexs
            print(row)
            params[row] = {
                "value": int(agregatedStats[label][row]),
                "time": int(time["date_timestamp"].values[0]),
            }
        else:
            params[row] = int(agregatedStats[label][row])
        data[label] = params


print(data)
