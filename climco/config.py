import os
from configparser import ConfigParser
from typing import Optional

# Helper functions

class PrintOnce:
    already_printed = False

    @classmethod
    def print(cls, to_print):
        if not cls.already_printed:
            print(to_print)

def load_parameter(param_name: str) -> Optional[str]:
    if param := load_from_env(param_name):
        return param
    if param := load_from_file(param_name):
        return param
    print(f"Could not find parameter '{param_name}'")


def load_from_env(param_name: str) -> Optional[str]:
    return os.environ.get(param_name.upper())


def load_from_file(param_name: str) -> Optional[str]:
    dir  = os.path.abspath(os.getcwd())
    path = os.path.join(dir, "backend.conf")
    PrintOnce.print(f"Config will be loaded from {path}")

    config = ConfigParser()
    config.read(path)

    try:
        if param := config["CONFIG"][param_name.upper()]:
            return param
    except KeyError: pass

# Actual config

class Config:
    SUPLA_CLIENT_ID = load_parameter("SUPLA_CLIENT_ID")
    SUPLA_CLIENT_SECRET = load_parameter("SUPLA_CLIENT_SECRET")
