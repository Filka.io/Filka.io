from climco.config import load_parameter
from flask import session, redirect, jsonify
import pandas as pd
from typing import Any
from climco import app, supla, db


@app.route("/login")
def login():
    redirect_uri = load_parameter("SUPLA_OAUTH_REDIRECT_URL")
    return supla.authorize_redirect(redirect_uri)


def get_user_info(token):
    url = "https://svr64.supla.org/api/v2.3.0/users/current"
    response = supla.get(url=url, token=token)
    response.raise_for_status()
    return response.json()


def insert_or_update_user(token):
    user_info = get_user_info(token)
    table = db().table("users")

    if table.get(user_info["id"]).run():
        table.get(user_info["id"]).update({"token": token}).run()
    else:
        user_info["token"] = token
        table.insert(user_info).run()

    return user_info["id"]


@app.route("/auth", methods=["GET"])
def auth():
    """OAuth2 for supla"""
    token = supla.authorize_access_token()
    session["user_key"] = insert_or_update_user(token)
    return redirect("/main")


@app.route("/logout")
def logout():
    session.pop("user", None)
    return redirect("/")


@app.route("/sensorList", methods=["GET"])
def printSensorList():
    """API For reading sensor list"""
    user_key = session.get("user_key")
    user_info = db().table("users").get(user_key).run()

    url = "https://svr64.supla.org/api/v2.3.0/channels?function=HUMIDITYANDTEMPERATURE"
    resp = supla.get(url=url, token=user_info["token"])

    resp.raise_for_status()
    sensors = resp.json()

    sensorInfo = {}
    for x in range(len(sensors)):
        sensorName = {"name": sensors[x]["caption"], "id": sensors[x]["id"]}
        sensorInfo[sensors[x]["id"]] = sensorName

    return jsonify(sensorInfo)


@app.route("/sensorID:<int:sensorID>/weatherData")
def getData(sensorID):
    """Transform data received from Supla-Cloud to Apexcharts format"""
    user_key = session.get("user_key")
    user_info = db().table("users").get(user_key).run()

    # reading logs
    numberOfLogs = 5000
    url = f"https://svr64.supla.org/api/v2.3.0/channels/{sensorID}/measurement-logs?limit={numberOfLogs}"
    resp = supla.get(url=url, token=user_info["token"])

    dataJSON = resp.json()
    if dataJSON is None:
        return jsonify([])
    else:
        data = [
            {"name": "humidity", "data": []},
            {"name": "temperature", "data": []},
        ]
        for element in dataJSON:
            time = int(element["date_timestamp"]) * 1000
            data[0]["data"].append(
                {
                    "x": time,
                    "y": float(element["humidity"]),
                }
            )
            data[1]["data"].append(
                {
                    "x": time,
                    "y": float(element["temperature"]),
                }
            )
        #            data[1]["data"].append(element["temperature"])

        # df = pd.DataFrame.from_records(data=dataJSON)
        # df['time'] = pd.to_datetime(df['date_timestamp'], unit='s')
        # date = df.to_json()
        return jsonify(data)


@app.route("/sensorID:<int:sensorID>/weatherStats/<int:hours>")
def getWeatherStats(hours, sensorID):
    """
    API for getting weather stats such as max,min,mean and std values
    (possible to extract other data as pandas describe() method)

    IMPORTANT INFO: The time value is calculated from your last sensor reading. Its done for debugging purposes, and its gonna be changed in the future

    TODO: Add reading actual epoch time
    TODO: add corr function from pandas
    """
    user_key = session.get("user_key")
    user_info = db().table("users").get(user_key).run()

    # reading logs
    numberOfLogs = 5000
    url = f"https://svr64.supla.org/api/v2.3.0/channels/{sensorID}/measurement-logs?limit={numberOfLogs}"
    resp = supla.get(url=url, token=user_info["token"])

    dataJSON = resp.json()
    if dataJSON is None:
        return jsonify([])
    else:
        """
        making DataFrame format
        """
        df = pd.DataFrame.from_records(dataJSON)
        timestamp: Any = df["date_timestamp"]  # type given to supress linting errors
        df["time"] = pd.to_datetime(timestamp, unit="s")
        df["date_timestamp"] = pd.to_numeric(df["date_timestamp"])
        df["temperature"] = pd.to_numeric(df["temperature"])
        df["humidity"] = pd.to_numeric(df["humidity"])
        df.set_index("date_timestamp")

        time = hours * 60 * 60
        stats = df.loc[
            df.date_timestamp > df.iloc[0]["date_timestamp"] - time, :
        ]  # reaching stats from desire time period

        dataVals = [
            "min",
            "max",
            "mean",
            "std",
            "median",
        ]  # desired values from ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max']

        agregatedStats = stats.agg({"temperature": dataVals, "humidity": dataVals})

        data = convertAgregatedStats(agregatedStats, df)
        return jsonify(data)


def convertAgregatedStats(agregatedStats, records):
    """converting stats to desired JSON"""
    data = {}
    for label in list(agregatedStats.columns):
        params = {}
        for row in agregatedStats.index:
            if row in ["max", "min"]:  # TODO: add idmax and idmin instead of it
                time = records.loc[
                    records[label] == agregatedStats[label][row]
                ]  # localize time for desired valuexs
                params[row] = {
                    "value": int(agregatedStats[label][row]),
                    "time": int(time["date_timestamp"].values[0]),
                }
            else:
                params[row] = int(agregatedStats[label][row])
                data[label] = params
    return data


@app.route("/healthcheck")
def printDataJSON():
    """api for testing"""
    return "API is alive"
