

# Welcome to Filka.io - Supla-based app to analyse and control  

App was created to analyze greenhouse parameters, as official app does not
meet these requirements.

## Filka.io main goals:

- Chart with humidity and temperature (for now chartscss, or just generated
  from matplotlib/ in the future interactive JS chart)
  - table with average/min/max/amplitude of humidity and temperature
  - recommended temperature/humidity values based on plants
  - integration with PlantCV
  - the journal

## What is working right now:

- The chart (more or less)
- Statistics (partially)
- Calendar (partially)

## Next features to be added:
- Weather forecasts
- Recommendations for growing plants based on weather and user's stats
- Shifting cultivation tips and planning
- Integration with PlantCV

## How to run this project

Install it.

```bash
  git clone https://gitlab.com/climco/climco.git
  cd climco
  make setup-dev
  . venv/bin/activate
  make # hit CTRL+C to quit
```

Open your favorite browser. Filka.io is now hosted on
[https://localhost/](https://localhost/). Before logging in you will have to
add OAuth tokens:

 1. Open supla.org, login into your account, and go to Account > Integrations
  > My OAuth apps > Register new OAuth application

 2. Into the name and description tabs you can write whatever you want

 3. Into authorization callback URLs put: https://localhost/api/auth/

 4. Click  Register a new OAuth application buttons

 5. Create `backend.conf` in project root folder and put info from the
  Configuration tab in Supla. You can look at `backend.conf.sample` for hints.

```
[CONFIG]
SUPLA_CLIENT_ID=YOUR PUBLIC ID FROM OAUTH APPLICATION TAB
SUPLA_CLIENT_SECRET=YOUR SECRET FROM OAUTH APPLICATION TAB
```

 6. Restart the application. Hit ctrl+c to quit the app and start it again with
  `make`.

The app was tested only under Debian GNU/Linux 11 (bullseye) and right now
under Debian GNU/Linux 12 (bookworm) on Firefox, Qutebrowser and Brave. We
don't test on Chrome.
